#define _WIN32_WINNT 0x500
#include <iostream>
#include <Windows.h>
#include <time.h>

using namespace std;

int main()
{
    HWND window = FindWindow(NULL, "Jeux instantanés - Mozilla Firefox");//nom de fenetre à adapter
    if (window)//si fenetre trouvé
    {
        RECT rect = {0};
        GetWindowRect(window, &rect);

        SetForegroundWindow(window);
        SetActiveWindow(window);
        SetFocus(window);
        Sleep(300);
        SetCursorPos(rect.right - 200, rect.bottom - 200);

        POINT pointDown;//servira à placer les deux points de click
        POINT pointUP;
        GetCursorPos(&pointDown);
        GetCursorPos(&pointUP);

        bool toogle = false;//pour ne capter qu'une fois le click souris

        cout << "Focus on windows' active" << endl;
        cout << "pls press R bfr W, then F" << endl;//j'ai pas essayé de pas suivre le conseil

        while(! (GetKeyState('F') & 0x8000) ){//Touche F termine

            while(GetKeyState('W') & 0x8000){//touche W (while) execute
                SetCursorPos(pointDown.x, pointDown.y);

                ///Appuie sur le click gauche
                INPUT Input = {0};
                Input.type = INPUT_MOUSE;
                Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
                ::SendInput(1,&Input,sizeof(INPUT));

                Sleep(200);//Pour le jeu ça change la vélocité de la balle
                SetCursorPos(pointUP.x, pointUP.y);

                ///Lâche le click gauche
                ::ZeroMemory(&Input,sizeof(INPUT));
                Input.type = INPUT_MOUSE;
                Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
                ::SendInput(1,&Input,sizeof(INPUT));

                Sleep(100);
            }
            while(GetKeyState('R') & 0x8000){//touche R (record)
                if((GetKeyState(VK_LBUTTON) & 0x100) != 0){//clic souris gauche
                    if(!toogle){
                        toogle = true;
                        GetCursorPos(&pointDown);
                        cout << "newPos for down" << endl;
                    }
                }else if(toogle){//quand clic laché
                    toogle = false;
                    GetCursorPos(&pointUP);
                    cout << "newPos for up" << endl;
                }
            }
        }

    }else{
        cout << "No window found" << endl;
        return 1;
    }

    cout << "Finish, F pressed" << endl;
    return 0;

}
